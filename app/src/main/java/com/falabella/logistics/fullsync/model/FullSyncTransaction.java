package com.falabella.logistics.fullsync.model;

import java.util.List;

import lombok.Data;

@Data
public class FullSyncTransaction {
    String number;
    String source;
    List<FullSyncTransactionReference> references;
}
