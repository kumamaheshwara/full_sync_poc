package com.falabella.logistics.fullsync.model;

import lombok.Data;

@Data
public class FullSyncTransactionReference {
    String type;
    String id;
}
