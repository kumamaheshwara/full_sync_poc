package com.falabella.logistics.fullsync.model;

import java.util.List;

import lombok.Data;

@Data
public class FullSyncNode {
    String nodeId;
    List<FullSyncNodeReferences> references;
    List<FullSyncNodeUniqueReferences> uniqueReferences;
}
