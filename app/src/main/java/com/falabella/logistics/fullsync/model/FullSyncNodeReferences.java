package com.falabella.logistics.fullsync.model;

import lombok.Data;

@Data
public class FullSyncNodeReferences {
    Integer id;
    String key;
    String value;
}
