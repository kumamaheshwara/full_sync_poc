package com.falabella.logistics.fullsync.model;

import lombok.Data;

@Data
public class FullSyncAttribute {
    String eventId;
    String eventType;
    String entityId;
    String entityType;
    String timestamp;
    String datetime;
    String version;
    String country;
    String commerce;
    String channel;
    String domain;
    String capability;
    String mimeType;
}
