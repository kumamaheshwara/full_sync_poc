package com.falabella.logistics.fullsync.model;

import lombok.Data;

@Data
public class FullSyncInventoryAction {
    FullSyncTransaction transaction;
    FullSyncItem item;
    String movedFrom;
    String movedTo;
    String reason;
    FullSyncQuantity quantity;
}
