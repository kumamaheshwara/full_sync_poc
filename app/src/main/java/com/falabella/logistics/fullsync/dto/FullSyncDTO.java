package com.falabella.logistics.fullsync.dto;

import lombok.Data;

@Data
public class FullSyncDTO {
    private final String logisticSkuId;
    private final String movedTo;
    private final Integer quantity;
}
