package com.falabella.logistics.fullsync.model;

import lombok.Data;

@Data
public class FullSyncQuantity {
    Integer quantityNumber;
    String quantityUOM;
}
