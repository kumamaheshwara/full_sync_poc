package com.falabella.logistics.fullsync.model;

import lombok.Data;

@Data
public class FullSyncPayload {
    FullSyncAttribute attributes;
    FullSyncData data;
}
