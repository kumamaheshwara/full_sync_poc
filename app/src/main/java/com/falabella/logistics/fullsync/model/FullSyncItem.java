package com.falabella.logistics.fullsync.model;

import lombok.Data;

@Data
public class FullSyncItem {
    String logisticSkuId;
    String sellerId;
    String sellerSkuId;
}
