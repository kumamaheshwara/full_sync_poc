package com.falabella.logistics.fullsync.model;

import lombok.Data;

@Data
public class FullSyncNodeUniqueReferences {
    String key;
    String value;
}
