package com.falabella.logistics.fullsync.model;

import java.util.List;

import lombok.Data;

@Data
public class FullSyncData {
    String transactionType;
    String transactionDateTime;
    FullSyncNode node;
    List<FullSyncInventoryAction> inventoryActionList;
}
